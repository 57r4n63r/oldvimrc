#! /bin/bash
install:
	cp vimrc ~/.vimrc
	cp -r vimrcs ~/.vim
	cp -r snippets ~/.vim
	cp -r after ~/.vim
	mkdir -p ~/.vim/tmp
	mkdir -p ~/.vim/tmp/undo
	mkdir -p ~/.vim/tmp/backup

.PHONY=install
