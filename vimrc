" Vim configuration
" Maintainer:   Marc-Antoine Loignon <info@lognoz.com>
" Last Change:  2018-06-08
" Version:      1.0.0

syntax on
filetype on

filetype plugin on
filetype indent on

source ~/.vim/vimrcs/default.vim
source ~/.vim/vimrcs/plugins.vim
source ~/.vim/vimrcs/colorscheme.vim
